import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *

import numpy as np

class ACoreSoftware(thesdk):
    def __init__(self,*arg): 
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 
        self.proplist = [];    # Properties that can be propagated from parent

    def init(self):
        pass

    def main(self):
        pass

    def run(self):
        pass