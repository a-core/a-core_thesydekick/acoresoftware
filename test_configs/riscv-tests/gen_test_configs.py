import os
from yaml import dump

tests = {
  "rv32mi": [
    "breakpoint",
    "csr",
    "illegal",
    "lh-misaligned",
    "lw-misaligned",
    "ma_addr",
    "ma_fetch",
    "mcsr",
    "sbreak",
    "scall",
    "sh-misaligned",
    "shamt",
    "sw-misaligned",
    "zicntr"
  ],
  "rv32ui": [
    "add",
    "addi",
    "and",
    "andi",
    "auipc",
    "beq",
    "bge",
    "bgeu",
    "blt",
    "bltu",
    "bne",
    #"fence_i",
    "jal",
    "jalr",
    "lb",
    "lbu",
    "lh",
    "lhu",
    "lui",
    "lw",
    #"ma_data",
    "or",
    "ori",
    "sb",
    "sh",
    "simple",
    "sll",
    "slli",
    "slt",
    "sltiu",
    "sltu",
    "sra",
    "srai",
    "srl",
    "srli",
    "sub",
    "sw",
    "xor",
    "xori"
  ],
  "rv32um": [
    "div",
    "divu",
    "mul",
    "mulh",
    "mulhu",
    "mulhsu",
    "mulhu",
    "rem",
    "remu"
  ]
}


hw_config_yaml = "$ACORECHIP/hw_configs/core/rv32im.yml"
test_program = "$ACORESOFTWARE/sw/riscv-tests"
sim_config_yaml = "$ACORETESTBENCHES/sim_configs/thesdk.yml"
sim_config = {"selfchecking": True, "timeout": 6000}
for test_case in tests:
  for test in tests[test_case]:
    test_name = f"{test_case}-p-{test}"
    test_config = {
      "hw_config_yaml": hw_config_yaml,
      "test_program": test_program,
      "sim_config_yaml": sim_config_yaml,
      "make_args": {
        "RISCV_TEST_NAME": test_name
      },
      "sim_config": sim_config
    }
    with open(f"{test_name}.yml", "w") as fd:
      fd.write("# Generated with gen_test_configs.py\n")
      dump(test_config, fd)

