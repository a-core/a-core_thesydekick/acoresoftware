A-Core Software
===============

This entity holds software programs for A-Core, and test configurations for running those programs.

.. toctree::
  :maxdepth: 2

  testconfig