Test config
===========

Example configuration for dhrystone

.. literalinclude:: dhrystone.yml
  :language: YAML